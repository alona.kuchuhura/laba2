package edu.hneu.mjt.kuchuhuralona.bankapi;

import edu.hneu.mjt.kuchuhuralona.dto.BankCard;
import edu.hneu.mjt.kuchuhuralona.dto.BankCardType;
import edu.hneu.mjt.kuchuhuralona.dto.User;

public interface Bank {
    BankCard createBankCard(User user, BankCardType bankCardType);
}
