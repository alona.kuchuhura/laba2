import edu.hneu.mjt.kuchuhuralona.bankapi.Bank;
import edu.hneu.mjt.kuchuhuralona.serviceapi.Service;

module lab {
    requires jmp.cloud.service.impl;
    requires jmp.dto;
    requires jmp.cloud.bank.impl;
    requires jmp.bank.api;
    requires jmp.service.api;
    uses Service;
    uses Bank;
}