package edu.hneu.mjt.kuchuhuralona.dto;

public enum BankCardType {
    CREDIT,
    DEBIT
}
