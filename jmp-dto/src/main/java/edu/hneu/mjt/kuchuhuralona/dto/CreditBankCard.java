package edu.hneu.mjt.kuchuhuralona.dto;

public class CreditBankCard extends BankCard {

    public CreditBankCard(User user, String cardNumber, double balance)
    {
        super(user, cardNumber, balance);
    }
}
