package edu.hneu.mjt.kuchuhuralona.dto;

import java.time.LocalDate;

public class Subscription {
    private String bankcard;
    private LocalDate startDate;

    public String getBankcard() {
        return bankcard;
    }

    public void setBankcard(String bankcard) {
        this.bankcard = bankcard;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
    @Override
    public String toString() {
        return getBankcard() + getStartDate();
    }
}
