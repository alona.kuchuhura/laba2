package edu.hneu.mjt.kuchuhuralona.cloudbankimpl;

import edu.hneu.mjt.kuchuhuralona.bankapi.Bank;
import edu.hneu.mjt.kuchuhuralona.dto.*;

import java.util.Random;

public class CloudBankImpl implements Bank {
    private final Random random = new Random();
    @Override
    public BankCard createBankCard(User user, BankCardType bankCardType) {
        return switch (bankCardType){
            case CREDIT -> new CreditBankCard(user, generateCardNumber(), random.nextDouble(150000));
            case DEBIT -> new DebitBankCard(user, generateCardNumber(), random.nextDouble(150000),
                    random.nextDouble(15000));
        };
    }

    public String generateCardNumber() {
        StringBuilder cardNumber = new StringBuilder();
        // Картки будуть віза
        cardNumber.append("4");
        for (int i = 1; i < 16; i++) {
            cardNumber.append(random.nextInt(10));
            // Для зручного відображення по 4 цифри
            if ((i + 1) % 4 == 0 && i != 14) {
                cardNumber.append(" ");
            }
        }
        return cardNumber.toString();
    }
}
