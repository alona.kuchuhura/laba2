## Лабораторна робота №2
___

**Тема:** "Дослідження додаткового функціоналу в Java 8-17+"

## Хід роботи

## 1) Створити maven проєкту з 5-ю модулями:
1. jmp-bank-api
2. jmp-service-api
3. jmp-cloud-bank-impl
4. jmp-cloud-service-impl
5. jmp-dto

**Створюємо проєкт**
![Screenshot1.png](images%2FScreenshot1.png)

**Створюємо модулі**
![Screenshot2.png](images%2FScreenshot2.png)
![Screenshot3.png](images%2FScreenshot3.png)
![Screenshot4.png](images%2FScreenshot4.png)
![Screenshot5.png](images%2FScreenshot5.png)
![Screenshot6.png](images%2FScreenshot6.png)

---
## 2) Створіть наступні класи в модулі jmp-dto:
1. [User]  
   String name;  
   String surmane;  
   LocalDate birthday;
2. [BankCard]  
   String number;  
   User user;
3. [Subscription]  
   String bankcard;  
   LocaleDate startDate;

**Створюємо класи в dto**

![Screenshot7.png](images%2FScreenshot7.png)
![Screenshot8.png](images%2FScreenshot8.png)
![Screenshot9.png](images%2FScreenshot9.png)
![Screenshot10.png](images%2FScreenshot10.png)
---
## 3) Продовжте клас BankCard за допомогою:

CreditBankCard  
DebitBankCard  

**Подовжуємо клас BankCard**
![Screenshot11.png](images%2FScreenshot11.png)
---

## 4) Створити enum :

[BankCardType]  
CREDIT  
DEBIT

**Створюємо Enum**

![Screenshot12.png](images%2FScreenshot12.png)
---

## 5) Додайте інтерфейс Bank до jmp-bank-api за допомогою:

BankCard createBankCard(User, BankCardType)

**Створюємо інтерфейс в заданому модулі**
![Screenshot13.png](images%2FScreenshot13.png)
---

## 6) Додайте module-info.java за допомогою:
requires jmp-dto  
export Bank interface

**Створюємо module-info в модулі jmp-bank-api**
![Screenshot14.png](images%2FScreenshot14.png)
---
## 7) Впровадити Bank в jmp-cloud-bank-impl. Метод повинен створювати новий клас в залежності від типу

**Створюємо відповідну карту за типом**
![Screenshot15.png](images%2FScreenshot15.png)
---

## 8) Додайте module-info.java який містить:  

requires transitive module with Bank interface  
requires jmp-dto  
export implementation of Bank interface

**Створюємо module-info в модулі jmp-cloud-bank-impl**
![Screenshot16.png](images%2FScreenshot16.png)
---

## 9) Додайте інтерфейс Service до jmp-service-api за допомогою:  

void subscribe(BankCard);  
Optional<Subscription> getSubscriptionByBankCardNumber(String);  
List<User> getAllUsers();

**Створюємо інтерфейс в відповідному модулі**
![Screenshot17.png](images%2FScreenshot17.png)
---

## 10) Додайте module-info.java за допомогою:  
requires jmp-dto  
export Services interface  

**Створюємо module-info в модулі jmp-service-api**
![Screenshot18.png](images%2FScreenshot18.png)
---

## 11) Впровадити Service в jmp-cloud-service-impl. API потоку користувача. Ви можете використовувати Map або Mongo/БД для перегляду даних.  

**Впроваджуємо Service в CloudServiceImpl**
![Screenshot19.png](images%2FScreenshot19.png)
![Screenshot20.png](images%2FScreenshot20.png)
---

## 12) Додайте module-info.java з налаштуваннями:
requires transitive module with Service interface  
requires jmp-dto  
export implementation of Service interfaces  

**Створюємо module-info в модулі jmp-cloud-service-impl**
![Screenshot21.png](images%2FScreenshot21.png)
---

## 13) Використовуйте var для визначення локальних змінних, де б це не було застосовано.
## 14) Використовуйте лямбди та функції Java 8, де б це не було застосовано.
## 15) Створити модуль основного додатку.

**Використання var для локальних змінних, використання лямбда, модуль основного проєкту**
![Screenshot22.png](images%2FScreenshot22.png)
![Screenshot23.png](images%2FScreenshot23.png)
---

## 16) Додайте module-info.java з налаштуваннями:  
use interface  
requires module with Bank implementation  
requires module with Service implementation  
requires jmp-dto  

**Створюємо module-info в головному модулі**
![Screenshot24.png](images%2FScreenshot24.png)
---

## 17) Приклад роботи проєкту:

![Screenshot25.png](images%2FScreenshot25.png)
![Screenshot26.png](images%2FScreenshot26.png)
---