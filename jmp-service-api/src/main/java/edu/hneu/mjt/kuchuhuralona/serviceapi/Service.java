package edu.hneu.mjt.kuchuhuralona.serviceapi;

import edu.hneu.mjt.kuchuhuralona.dto.BankCard;
import edu.hneu.mjt.kuchuhuralona.dto.Subscription;
import edu.hneu.mjt.kuchuhuralona.dto.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface Service {
    void subscribe(BankCard bankCard, LocalDate startDate);

    Optional<Subscription> getSubscriptionByBankCardNumber(String bankCardNumber);

    List<User> getAllUsers();
    List<BankCard> getAllCards();

}
