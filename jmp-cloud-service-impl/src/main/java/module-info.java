module jmp.cloud.service.impl {
    requires transitive jmp.service.api;
    requires jmp.dto;
    exports edu.hneu.mjt.kuchuhuralona.cloudserviceimpl;
}